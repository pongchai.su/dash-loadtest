package main

import (
        "fmt"
        "io/ioutil"
        "net/http"
        "encoding/xml"
        "strings"
        "strconv"
        "log"
        "time"
        "io"
        "os"
)

type MPD struct {
        Location string `xml:"Location"`
        Periods []Period `xml:"Period"`
}

type Period struct {
        AdaptationSets []AdaptationSet `xml:"AdaptationSet"`
}

type AdaptationSet struct {
        SegmentTemplates []SegmentTemplate `xml:"SegmentTemplate"`
        Representation Representation `xml:"Representation"`
}

type Representation struct {
        ID string `xml:"id,attr"`
}

type SegmentTemplate struct {
        Media string `xml:"media,attr"`
        Initialization string `xml:"initialization,attr"`
        SegmentTimeline SegmentTimeline `xml:"SegmentTimeline"`
}

type SegmentTimeline struct {
        Segments []Segment `xml:"S"`
}

type Segment struct {
        T uint64 `xml:"t,attr"`
        D uint64 `xml:"d,attr"`
}

func requestMedia(client *http.Client,data []byte, init bool) string {
        var mpd MPD
        if err := xml.Unmarshal(data, &mpd); err != nil {
                log.Fatal(err)
        }

        //fmt.Println("Location:", mpd.Location)
        parts := strings.Split(mpd.Location, "/")
        frontUrl := ""
        if len(parts) > 4 {
                frontUrl = parts[0] + "//" + parts[2] + "/" + parts[3] + "/" + parts[4] + "/"

        } else {
                fmt.Println("URL does not contain enough parts")
        }

        AdaptationSetIndex := 0
        for _, periods := range mpd.Periods {
                for _, adaptationset := range periods.AdaptationSets {
                        if AdaptationSetIndex < 2 {
                                representation := adaptationset.Representation.ID
                                for _, segTemp := range adaptationset.SegmentTemplates {
                                        if init {
                                                InitRep := strings.Replace(segTemp.Initialization, "$RepresentationID$", representation, 1)
                                                requestMediafile(client,frontUrl + InitRep)
                                        }
                                        var segtime uint64 = 0
                                        for _, segment := range segTemp.SegmentTimeline.Segments {
                                                segtime +=  segment.T
                                                segtime +=  segment.D
                                                //fmt.Println("T:", segment.T)
                                                //fmt.Println("D:", segment.D)
                                        }
                                        MediaRep := strings.Replace(segTemp.Media, "$RepresentationID$", representation, 1)
                                        MediaTime := strings.Replace(MediaRep, "$Time$", strconv.FormatUint(segtime,10), 1)
                                        //fmt.Println("MediaURL:", MediaTime)
                                        requestMediafile(client,frontUrl + MediaTime)
                                }
                                AdaptationSetIndex += 1
                        }
                }
        }
        return mpd.Location
}

func requestURL(client *http.Client, url string) ([]byte, error) {
        // Create an HTTP request
        req, err := http.NewRequest(http.MethodGet, url, nil)
        if err != nil {
                return nil, fmt.Errorf("failed to create request for %s: %v", url, err)
        }

        // Send the request
        resp, err := client.Do(req)
        if err != nil {
                return nil, fmt.Errorf("failed to send request to %s: %v", url, err)
        }
        defer resp.Body.Close()

        // Read the response body
        body, err := ioutil.ReadAll(resp.Body)
        if err != nil {
                return nil, fmt.Errorf("failed to read response body from %s: %v", url, err)
        }

        return body, nil
}

func requestMediafile(client *http.Client, url string) {
        // Create a new HTTP GET request
        req, err := http.NewRequest("GET", url, nil)
        if err != nil {
                fmt.Printf("Error creating request: %s\n", err)
                return
        }

        // Send the request
        resp, err := client.Do(req)
        if err != nil {
                fmt.Printf("Error sending request: %s\n", err)
                return
        }
        defer resp.Body.Close()

        // Discard the response body
        _, err = io.Copy(io.Discard, resp.Body)
        if err != nil {
                fmt.Printf("Error discarding body: %s\n", err)
                return
        }

        //fmt.Println("File downloaded and discarded successfully.")
}

func main() {

        data, err := ioutil.ReadFile("targetURL.txt")
        if err != nil {
                log.Fatal(err)
        }
        urls := strings.Split(string(data), "\n")
        url := urls[0]

        // Create an HTTP client with HTTP/2 support
        client := &http.Client{
                Transport: &http.Transport{
                        ForceAttemptHTTP2:   true,
                        MaxIdleConns:        100,
                        MaxIdleConnsPerHost: 100,
                },
        }

        // Enable keep-alive
        //req.Header.Set("Connection", "keep-alive")

        // loop for x time
        // Check if the number argument is provided
        if len(os.Args) < 2 {
                log.Fatal("Please provide a number argument")
        }

        // Parse the number argument
        num, err := strconv.Atoi(os.Args[1])
        if err != nil {
                log.Fatal("Invalid number argument:", err)
        }
        loadinit := true
        fmt.Println("LoadInit:",1)

        // Please input Segment Duration in seconds
        segmentduration := 2 * time.Second

        for i := 1; i < num+1; i++ {
                startTime := time.Now()
                // Send the request and handle the response
                body, err := requestURL(client, url)
                if err != nil {
                        fmt.Printf("Failed to request %s: %v\n", url, err)
                } else {
                        // Request Media and replace url with url in <Location> tag
                        url = requestMedia(client,body,loadinit)
                        fmt.Println("LoadMedia:", i)
                        loadinit = false
                }
                endTime := time.Now()
                runtime := endTime.Sub(startTime)
                remaintime := segmentduration - runtime
                if remaintime > 0 {
                        time.Sleep(remaintime)
                }
        }
}
